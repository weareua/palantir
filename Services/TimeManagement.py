import time
import random
import telegram


def typing_time(bot, chat_id):
    seconds_list = [1, 2, 3]
    bot.sendChatAction(
        chat_id=chat_id, action=telegram.ChatAction.TYPING)
    time.sleep(random.choice(seconds_list))
