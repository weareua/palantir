import urllib
import json
import random
import requests


def get_image(query):
    headers = {
        # Request headers
        'Content-Type': 'multipart/form-data',
        'Ocp-Apim-Subscription-Key': "d0e931c9a6894691b2633e5d7544d551",
    }

    params = urllib.parse.urlencode({
        "q": query,
        "count": "5",
    })

    try:
        response = requests.post(
            'https://api.cognitive.microsoft.com/bing/v5.0/images/search?%s' %
            params, headers=headers)
        data = json.loads(response.content.decode())
        actual_url = data['value'][random.randint(0, 4)]['contentUrl']
        return actual_url
    except Exception as e:
        # TODO: handle it in a proper way
        print("[Errno {0}] {1}".format(e.errno, e.strerror))