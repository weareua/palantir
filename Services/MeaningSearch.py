import requests


def get_meaning(query, lang):
    params = {
        "action": "query",
        "format": "json",
        "titles": query,
        "prop": "extracts",
        "exintro": True,
        "explaintext": True,
    }

    try:
        response = requests.get(
            'https://%s.wikipedia.org/w/api.php' % lang,
            params=params).json()
        page = next(
            iter(response['query']['pages'].values()))
        return page['extract']
    except Exception as e:
        # TODO: handle it in a proper way
        return "[Errno {0}] {1}".format(e.errno, e.strerror)
