import random
from Components.Lingua import greetings_list, thx_list
from Services.MeaningSearch import get_meaning
from Services.TimeManagement import typing_time
from Services.ImageSearch import get_image


def start(bot, update):
    chat_id = update.message.chat_id
    start_text = "Мої вітання, {0}. ".format(
        update.message.from_user.first_name) + "\n" + \
        "Моя головна функція - дублювати новини з публічної" + \
        " сторінки Твоєї Фішки. Також з різним рівнем успіху я можу" + \
        " дати визначення деяким відомим мені термінам." + "\n" + \
    typing_time(bot, chat_id)
    bot.sendMessage(
        chat_id=update.message.chat_id,
        text=start_text)


def hi(bot, update):
    chat_id = update.message.chat_id
    attr_list = [
        random.choice(greetings_list), update.message.from_user.first_name]
    random.shuffle(attr_list)
    greetings_text = "{0}, {1}!".format(attr_list[0], attr_list[1])
    if not greetings_text[0].isupper():
        greetings_text = greetings_text[:1].capitalize() + greetings_text[1:]

    typing_time(bot, chat_id)
    bot.sendMessage(chat_id=chat_id, text=greetings_text)


def thx(bot, update):
    chat_id = update.message.chat_id
    attr_list = [
        random.choice(thx_list), update.message.from_user.first_name]
    random.shuffle(attr_list)
    thx_text = "{0}, {1}.".format(attr_list[0], attr_list[1])
    if not thx_text[0].isupper():
        thx_text = thx_text[:1].capitalize() + thx_text[1:]

    typing_time(bot, chat_id)
    bot.sendMessage(
        chat_id=update.message.chat_id,
        text=thx_text)


def meaning(bot, update, actual_query, lang):
    chat_id = update.message.chat_id
    err_text = 'На жаль, мені ' + \
        'не вдалося знайти жодного запису з цього приводу.'
    try:
        typing_time(bot, chat_id)
        text = get_meaning(actual_query, lang)
        if not text:
            text = err_text
        bot.sendMessage(
            chat_id=chat_id,
            text=text)
    except Exception:
        # TODO: handle it in a proper way
        typing_time(bot, chat_id)
        bot.sendMessage(
            chat_id=chat_id,
            text=err_text)

def pic(bot, update, actual_query):
    chat_id = update.message.chat_id
    err_text = 'На жаль, мені ' + \
        'не вдалося знайти жодного запису з цього приводу.'
    try:
        bot.sendPhoto(
            chat_id=chat_id,
            photo=get_image(actual_query))
    except Exception:
        # TODO: handle it in a proper way
        typing_time(bot, chat_id)
        bot.sendMessage(
            chat_id=chat_id,
            text=err_text)
